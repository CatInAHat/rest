package rest.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.RepositoryCatalog;
import dao.iRepositoryCatalog;
import model.Person;
import rest.dto.PersonSummaryDto;


@Path("person")
public class Service {

    iRepositoryCatalog repository = new RepositoryCatalog();
    int currentId = 0;

    @PUT
    @Path("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response add(PersonSummaryDto dto){

        Person person = new Person();
        person.setId(currentId);
        person.setAddress(dto.getAddress());
        person.setAge(dto.getAge());
        person.setDescription(dto.getDescription());
        person.setEducation(dto.getEducation());
        person.setEyeColor(dto.getEyeColor());
        person.setHair(dto.getHair());
        person.setUsername(dto.getUsername());
        person.setHeight(dto.getHeight());
        repository.profiles().add(person);

        currentId ++;
        return Response.status(201).build();
    }

    @GET
    @Path("/{personId}")
    @Produces("application/json")
    public Person get(@PathParam("personId") int id){
        Person person = repository.profiles().get(id);
        if (person == null) {
            return new Person();
        }
        return person;
    }
    @DELETE
    @Path("/")
    @Produces("application/json")
    public Response delete(@PathParam("personId") int id, PersonSummaryDto dto){
        Person person = new Person();
        person.setId(id);
        person.setAddress(dto.getAddress());
        person.setAge(dto.getAge());
        person.setDescription(dto.getDescription());
        person.setEducation(dto.getEducation());
        person.setEyeColor(dto.getEyeColor());
        person.setHair(dto.getHair());
        person.setUsername(dto.getUsername());
        person.setHeight(dto.getHeight());
        repository.profiles().delete(person);
        return Response.status(200).build();
    }

    @GET
    @Path("/all")
    @Produces("application/json")
    public List<Person> getAll(){
        return repository.profiles().getAll();
    }
}