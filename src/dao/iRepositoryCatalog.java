package dao;

import model.Person;

public interface iRepositoryCatalog {

    public iRepository<Person> profiles();

}