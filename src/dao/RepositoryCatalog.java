package dao;

import model.Person;

public class RepositoryCatalog implements iRepositoryCatalog {
    
	public iRepository<Person> profiles() {
        return new ProfileRepository();
    }
}