package dao;

import java.util.ArrayList;
import java.util.List;

import model.Person;

public class ProfileRepository implements iRepository<Person> {

    private static List<Person> profiles = new ArrayList<Person>();
    private int currentId=1;

    public void add(Person person) {
        person.setId(currentId);
        profiles.add(person);
        currentId++;
    }

    public void delete(Person person) {
        profiles.remove(person);

    }

    public Person get(int id) {
        for (Person current : profiles) {
            if (current.getId() == id)
                return current;
        }
        return null;
    }
    public List<Person> getAll() {
        return profiles;
    }
}